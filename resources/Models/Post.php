<?php

namespace Theme\Models;

use Illuminate\Database\Eloquent\Model;
use \WP_Query;

/**
 * Class Post.
 * Help you retrieve data from your $prefix_posts table.
 *
 * @package Theme\Models
 */
class Post extends Model
{
  public function get_fonction() {
    $args = array(
      'post_type' => 'fonction',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'order' => 'ASC'
    );

    $posts = new WP_Query($args);

    $fonction = array();
    foreach ($posts->get_posts() as $key => $val) {
      $fonction[$key]['title'] = get_the_title($val->ID);
      $fonction[$key]['titre'] = get_post_meta($val->ID, 'th_titre', true);
      $fonction[$key]['description'] = get_post_meta($val->ID, 'th_description', true);
      $fonction[$key]['image'] = get_post_meta($val->ID, 'th_image', true);
    }

    return $fonction;
  }

  public function get_metier() {
    $args = array(
      'post_type' => 'slug-metier',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'order' => 'ASC'
    );

    $posts = new WP_Query($args);

    $metier = array();
    foreach ($posts->get_posts() as $key => $val) {
      $metier[$key]['title'] = get_the_title($val->ID);
      $metier[$key]['metier_name'] = get_post_meta($val->ID, 'th_metier_name', true);
      $metier[$key]['metier_description'] = get_post_meta($val->ID, 'th_metier_description', true);
      $metier[$key]['metier_logo'] = get_post_meta($val->ID, 'th_metier_logo', true);
      $metier[$key]['metier_classe'] = get_post_meta($val->ID, 'th_metier_classe', true);
      $metier[$key]['metier_recrutement'] = get_post_meta($val->ID, 'th_metier_recrutement', true);
    }

    return $metier;
  }
}
