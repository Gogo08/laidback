<?php

/**
 * home-application.php - Custom code template home.
 */

use Themosis\Support\Facades\Field;
use Themosis\Support\Facades\Metabox;
use Themosis\Support\Section;

add_action('init', 'custom_page_home');
function custom_page_home() {
	// Get the Post ID.
	if(isset($_GET['post']))
		$post_id = $_GET['post'];
	else if(isset($_POST['post_ID']))
		$post_id = $_POST['post_ID'];

	if(!isset($post_id) || empty($post_id))
		return;

	// Get the name of the Page Template file.
	$template_file = get_post_meta($post_id, '_wp_page_template', true);

	// Do something for the template
	if($template_file == "home") {
		remove_post_type_support('page','author');
		remove_post_type_support('page','custom-fields');
		remove_post_type_support('page','comments');
		remove_post_type_support('page','excerpt' );
		remove_post_type_support('page','trackbacks');
		remove_post_type_support('page','editor');
		remove_post_type_support('page','revisions');
	}
}

Metabox::make('Home', 'page')
	->add(new Section('top', 'Haut de la page', [
			Field::media('home_background', ['label' => 'Background', 'type'  => 'image']),
			Field::media('logo', ['label' => 'Logo', 'type'  => 'image']),
			Field::text('server_name', ['label' => 'Nom du serveur']),
			Field::text('serveur_description', ['label' => 'Description du serveur']),
	]))

	->add(new Section('rejoindre', 'Nous rejoindre', [
			Field::text('titre_rejoindre', ['label' => 'Titre Rejoindre']),
			Field::text('discord_button', ['label' => 'Texte bouton Discord']),
			Field::text('discord', ['label' => 'Lien Discord']),
			Field::text('discord_info', ['label' => 'Info Discord']),
			Field::text('fivem_button', ['label' => 'Texte bouton FiveM']),
			Field::text('fivem', ['label' => 'Lien FiveM']),
			Field::text('fivem_info', ['label' => 'Info FiveM']),
	]))
	->setTemplate('home')
	->setTitle('Contenu de la page')
	->set();
