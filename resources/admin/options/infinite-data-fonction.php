<?php
namespace App\Hooks;

use Themosis\Support\Facades\Field;
use Themosis\Support\Facades\Metabox;
use Themosis\Hook\Hookable;
use Themosis\Support\Facades\PostType;

PostType::make('fonction', 'Fontionnalités', 'Fontionnalités')
    ->setArguments([
		'label'	=>  'Gestion Fontionnalités',
		'public' => true,
		'show_ui' => true,
		'supports' => ['title'],
		'rewrite' => true,
		'show_in_menu'	=> "plugin_options",
		'query_var' => true,
		'labels' => ""
    ])
	->setTitlePlaceholder('Ajouter Fontionnalité')
	->set();


  Metabox::make('fonctionnalite', 'fonction')
  ->add(Field::text('titre', ['label' => 'Titre']))
  ->add(Field::text('description', ['label' => 'Description']))
  ->add(Field::media('image', ['label' => 'Image', 'type'  => 'image']))


	->addTranslation('done', 'Enregistrement effectué !')
	->addTranslation('error', 'Veuillez vérifier que les champs ont été correctement complétés')
	->addTranslation('submit', 'Enregistrer')
	->setTitle('Fontionnalités')
	->set();
