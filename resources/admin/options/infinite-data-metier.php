<?php
namespace App\Hooks;

use Themosis\Support\Facades\Field;
use Themosis\Support\Facades\Metabox;
use Themosis\Hook\Hookable;
use Themosis\Support\Facades\PostType;

PostType::make('slug-metier', 'Métiers', 'Metiers')
    ->setArguments([
		'label'	=>  'Gestion Métiers',
		'public' => true,
		'show_ui' => true,
		'supports' => ['title'],
		'rewrite' => true,
		'show_in_menu'	=> "plugin_options",
		'query_var' => true,
		'labels' => ""
    ])
	->setTitlePlaceholder('Ajouter Métier')
	->set();


  Metabox::make('metier', 'slug-metier')
  ->add(Field::text('metier_name', ['label' => 'Nom du métier']))
  ->add(Field::text('metier_description', ['label' => 'Description du métier']))
  ->add(Field::media('metier_logo', ['label' => 'Logo', 'type'  => 'image']))
  ->add(Field::choice('classe', [
      'choices' => ['Services Publics', 'Whitelist', 'Pôle Emploi', 'Gangs / Organisations']
    ]))
  ->add(Field::choice('recrutement', [
      'choices' => ['ON', 'OFF']
    ]))


	->addTranslation('done', 'Enregistrement effectué !')
	->addTranslation('error', 'Veuillez vérifier que les champs ont été correctement complétés')
	->addTranslation('submit', 'Enregistrer')
	->setTitle('Métiers')
	->set();
