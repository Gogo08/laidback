<?php
use Themosis\Support\Facades\Field;
use Themosis\Support\Facades\Page;
use Themosis\Support\Section;

$page = Page::make('page-theme-footer', 'Footer')->set();

$page->addSections([
  new Section('section-slug-adresse', 'Adresse'),
  new Section('section-slug-newsletter', 'Newsletter'),
  new Section('section-slug-rs', 'Réseaux sociaux')
]);

$page->addSettings([
    'section-slug-adresse' => [
      Field::text('titre', ['label' => 'Titre']),
			Field::text('adresse', ['label' => 'Adresse']),
      Field::text('ville', ['label' => 'Ville'])
    ],
    'section-slug-rs' => [
      Field::text('twitter', ['label' => 'Twitter']),
      Field::text('discord', ['label' => 'Discord'])
    ]
]);
