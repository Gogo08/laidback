@extends('layouts.main')

@section('styles')

@endsection

@section('content')
	<section id="top" >
			<div class="uk-container">
				<div class="uk-text-center">
					<h2>{!! $metier_titre !!}</h2>

					<div class="divider-custom divider-light">
						<div class="divider-custom-line"></div>
						<div class="divider-custom-icon"><i class="fab fa-ethereum"></i></i></div>
						<div class="divider-custom-line"></div>
					</div>

				</div>
			</div>
	</section>

	<section id="service">
		<div class="uk-container">

		</div>
	</section>
@endsection

@section('javascript')

@endsection
