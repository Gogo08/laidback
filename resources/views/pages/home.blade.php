@extends('layouts.main')

@section('styles')

@endsection

@section('content')
	<section id="top" >
			<div class="uk-container">
				<div class="uk-text-center">
					<img class="logo" src="{{ wp_get_attachment_image_src($logo, 'logo')[0] }}" alt="LaidBack RolePlay">

					<h1>{!! $server_name !!}</h1>

					<div class="divider-custom divider-light">
						<div class="divider-custom-line"></div>
						<div class="divider-custom-icon"><i class="fab fa-ethereum"></i></i></div>
						<div class="divider-custom-line"></div>
					</div>

					<h3>{!! $serveur_description !!}</h3>
				</div>
			</div>
	</section>

	<section id="fonctionnalite">
		<div class="uk-container">
			<div class="uk-text-center">
				<h2>Fonctionnalités</h2>

				<div class="divider-custom">
					<div class="divider-custom-line"></div>
					<div class="divider-custom-icon"><i class="fab fa-ethereum"></i></i></div>
					<div class="divider-custom-line"></div>
				</div>
			</div>

			<div class="fonction" uk-grid>
				@foreach($list_fonction as $key => $item)
					<div class="uk-width-1-3">
						<div class="uk-text-center">
							<div class="photo">
								<img class="" src="{{ wp_get_attachment_image_src($item['image'], 'fonctionnalite')[0] }}">
							</div>
							<h2>{{ $item['titre'] }}</h2>
							<p>{{ $item['description'] }}</p>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</section>

	<section id="rejoindre">
		<div class="uk-container">
			<div class="uk-text-center">
				<h2>{!! $titre_rejoindre !!}</h2>

				<div class="divider-custom">
					<div class="divider-custom-line"></div>
					<div class="divider-custom-icon"><i class="fab fa-ethereum"></i></i></div>
					<div class="divider-custom-line"></div>
				</div>
			</div>
		</div>
		<div class="uk-container">
			<div class="uk-text-center">
				<div class="rejoindre_bouton uk-grid-column-small uk-grid-row-large uk-child-width-1-2@s uk-text-center" uk-grid>

					<div>
						<div class="button">
							<a class="uk-button uk-button-default" href="{{ $discord }}"><i class="fab fa-discord"></i>{{ $discord_button }}</a>
						</div>

						<div class="uk-text-center">
							<p>{!! $discord_info !!}</p>
						</div>

					</div>

					<div>
						<div class="button">
							<a class="uk-button uk-button-default" href="{{ $fivem }}"><i class="fas fa-gamepad"></i>{{ $fivem_button }}</a>
						</div>

						<div class="uk-text-center">
							<p>{!! $fivem_info !!}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('javascript')

@endsection
