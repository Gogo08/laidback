<header id="page-header">
  <div id="nav" uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
    <div class="uk-container">

      <nav class="uk-navbar-container uk-margin uk-navbar-transparent" uk-navbar>
        <div class="uk-navbar-left">

          <a class="uk-navbar-item uk-logo" href="{{ get_home_url() }}">LaidBack</a>

          <div class="uk-navbar-item">
            {!! wp_nav_menu(['theme_location' => 'nav', 'container' => false, 'menu_id' => 'main-nav']) !!}
          </div>

        </div>
      </nav>
    </div>
  </div>

</header>
