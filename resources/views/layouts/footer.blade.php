<footer id="page-footer">

  <div id="prefooter">
    <div class="uk-container">
      <div class="uk-child-width-1-2 uk-text-center" uk-grid>
        <div class="uk-text-center">
          <h4>Localisation</h4>
          <p>{!! get_option('th_adresse') !!}<br>
          {!! get_option('th_ville') !!}</p>
        </div>

        <div class="social uk-text-center">
          <h4>LaidBack aussi sur</h4>
          <a href="{!! get_option('th_twitter') !!}" class="uk-icon-button uk-margin-small-right"><i class="fab fa-twitter"></i></a>
          <a href="{!! get_option('th_discord') !!}" class="uk-icon-button  uk-margin-small-right"><i class="fab fa-discord"></i></a>
          <!-- <a href="" class="uk-icon-button  uk-margin-small-right"><i class="fab fa-youtube"></i></a> -->
        </div>
      </div>
    </div>
  </div>

  <div id="copyright">
    <div class="uk-container">
      <p>© <?php echo date ('Y'); ?> – {!! get_option('th_titre') !!}. Tous droits réservés. <a href="#" rel="nofollow">Mentions légales.</a></p>
    </div>
  </div>

</footer>
