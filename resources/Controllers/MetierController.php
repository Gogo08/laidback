<?php

namespace Theme\Controllers;

use Theme\Models\Post;
use Illuminate\Routing\Controller as BaseController;

class MetierController extends BaseController
{
    // Haut de page
    protected $metier_titre;

    public function __construct() {

      // Haut de page
      $this->metier_titre = get_post_meta(get_the_ID(), 'th_metier_titre', true);
    }

    public function index(Post $model) {
  	  return view('pages.metier', [
        // Haut de page
        'metier_titre' => $this->metier_titre,
      ]);
    }
}
