<?php

namespace Theme\Controllers;

use Theme\Models\Post;
use Illuminate\Routing\Controller as BaseController;

class HomeController extends BaseController
{
    // Haut de page
    protected $home_background;
    protected $logo;
    protected $server_name;
    protected $serveur_description;

    // Nous rejoindre
    protected $titre_rejoindre;
    protected $discord_button;
    protected $discord;
    protected $discord_info;
    protected $fivem_button;
    protected $fivem;
    protected $fivem_info;

    public function __construct() {

      // Haut de page
      $this->home_background = get_post_meta(get_the_ID(), 'th_home_background', true);
      $this->logo = get_post_meta(get_the_ID(), 'th_logo', true);
      $this->server_name = get_post_meta(get_the_ID(), 'th_server_name', true);
      $this->serveur_description = get_post_meta(get_the_ID(), 'th_serveur_description', true);

      // Nous rejoindre
      $this->titre_rejoindre = get_post_meta(get_the_ID(), 'th_titre_rejoindre', true);
      $this->discord_button = get_post_meta(get_the_ID(), 'th_discord_button', true);
      $this->discord = get_post_meta(get_the_ID(), 'th_discord', true);
      $this->discord_info = get_post_meta(get_the_ID(), 'th_discord_info', true);
      $this->fivem_button = get_post_meta(get_the_ID(), 'th_fivem_button', true);
      $this->fivem = get_post_meta(get_the_ID(), 'th_fivem', true);
      $this->fivem_info = get_post_meta(get_the_ID(), 'th_fivem_info', true);
    }

    public function index(Post $model) {
  	  return view('pages.home', [
        // Haut de page
        'home_background' => $this->home_background,
        'logo' => $this->logo,
        'server_name' => $this->server_name,
        'serveur_description' => $this->serveur_description,

        // Nous rejoindre
        'titre_rejoindre' => $this->titre_rejoindre,
        'discord_button' => $this->discord_button,
        'discord' => $this->discord,
        'discord_info' => $this->discord_info,
        'fivem_button' => $this->fivem_button,
        'fivem' => $this->fivem,
        'fivem_info' => $this->fivem_info,

        // Post
        'list_fonction' =>  $model->get_fonction(),
      ]);
    }
}
